# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - QR code component solution](#frontend-mentor---qr-code-component-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [Screenshot](#screenshot)
      - [Desktop (1444px):](#desktop-1444px)
      - [Mobile (375px):](#mobile-375px)

## Overview

### Screenshot

#### Desktop (1444px):

![image info](./images/qr_code_1444px.png)

#### Mobile (375px):

![image info](./images/qr_code_375px.png)